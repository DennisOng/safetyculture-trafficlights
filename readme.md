# Traffic Light Simulator

## High-level Description
You are required to provide the code for an application that simulates a set of traffic lights at
an intersection. 

The traffic lights are designated (N, S) and (E, W) like a compass.

## Requirements
* When switching from green to red, the yellow light must be displayed for 30 seconds prior to
it switching to red. The lights will change automatically every 5 minutes.
* You're not required to optimize the solution, just focus on a functional approach to
requirements.
* Provide the output for the light changes which occur during a given thirty minute period.
* You must provide unit tests for all logic.
* Create a repo on bitbucket/github and provide the link as well as instructions on how to run.

## Installation Notes
1. Install Node.js (tested for node v8.3.0)
2. Install npm packages
```
npm install
```

## Begin Simulation
1. From the root dir, run:
```
node app.js
```
2. Simulation output will be dumped on the console

## Test Notes
1. To run unit tests, run:
```
npm test
```

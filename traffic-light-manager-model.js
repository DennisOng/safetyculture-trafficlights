const StateMachine = require('javascript-state-machine');

/**
 * Model for Traffic Light Manager
 * Parent for the Traffic Light Model, responsible for coordinating state changes between different sets of traffic lights
 *
 * State Change Diagram:
 *
 *      GR -> YR -> RG -> RY
 *       ^                 |
 *       |                 |
 *       ------------------
 *
 * First char represents status of the first set of trafficlight (eg: NS) while second char represents the status of the second set (eg: EW)
 */

const TrafficLightManager = StateMachine.factory({
    data: function(trafficLightNS, trafficLightEW, isAutoSwitchEnabled) {
        return {
            trafficLightNS: trafficLightNS,
            trafficLightEW: trafficLightEW,
            isAutoSwitchEnabled: isAutoSwitchEnabled
        }
    },
    transitions: [
        { name: 'toYellowRed', from: ['none','greenRed'],  to: 'yellowRed' },
        { name: 'toRedGreen', from: 'yellowRed', to: 'redGreen' },
        { name: 'toRedYellow', from: 'redGreen', to: 'redYellow' },
        { name: 'toGreenRed', from: 'redYellow', to: 'greenRed' },
    ],
    methods: {
        init: function() {
            this.toYellowRed();
        },
        onToYellowRed: function() {
            this.trafficLightEW.toRed();
            this.trafficLightNS.toYellow();
        },
        onToRedGreen: function() {
            this.trafficLightNS.toRed();
            this.trafficLightEW.toGreen();
        },
        onToRedYellow: function() {
            this.trafficLightNS.toRed();
            this.trafficLightEW.toYellow();
        },
        onToGreenRed: function() {
            this.trafficLightEW.toRed();
            this.trafficLightNS.toGreen();
        },
        onEnterState: function(lifecycle) {
            if (this.isAutoSwitchEnabled){
                let duration;
                switch (this.state){
                    case 'yellowRed':
                    case 'redYellow':
                        duration = 30000;
                        break;
                    case 'redGreen':
                    case 'greenRed':
                        duration = 270000;
                        break;
                }
                setTimeout(() => {
                    this[this.nextState()]();
                }, duration);
            }
        },
        nextState: function() {
            if (this.transitions().length !== 1) {
                return new Error("More than 1 possible next state")
            }
            return this.transitions()[0];
        }
    }
});

module.exports.TrafficLightManager = TrafficLightManager;
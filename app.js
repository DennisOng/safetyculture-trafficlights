const TrafficLight = require('./traffic-light-model').TrafficLight;
const TrafficLightManager = require('./traffic-light-manager-model').TrafficLightManager;

// Init traffic light manager (passing in both instances of traffic lights)
const trafficLightNS = new TrafficLight('(N, S)');
const trafficLightEW = new TrafficLight('(E, W)');
const trafficLightManager = new TrafficLightManager(trafficLightNS, trafficLightEW, true);

// Observers to log when each traffic light enters a new state
trafficLightNS.observe('onEnterState', function() {
    console.log(trafficLightNS.describe());
});
trafficLightEW.observe('onEnterState', function() {
    console.log(trafficLightEW.describe());
});

// Begin Simulation
trafficLightManager.init();

// Stop Simulation after 30 mins
setTimeout(() => {
    trafficLightManager.isAutoSwitchEnabled = false;
}, 1800000);
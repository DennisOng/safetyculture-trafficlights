const sinon = require('sinon');
const expect = require('chai').expect;
const TrafficLight = require('../traffic-light-model').TrafficLight;
const TrafficLightManager = require('../traffic-light-manager-model').TrafficLightManager;
const moment = require('moment');

describe('Traffic Light Simulator', function(){
    describe("Traffic Light", function() {

        let trafficLight;

        beforeEach(function(){
            trafficLight = new TrafficLight('NS');
        });

        it("should store it's position", function() {
            expect(trafficLight.position).to.equal('NS');
        });

        it("should transition between allowed states", function() {
            trafficLight.toGreen(); expect(trafficLight.state).equal('green');
            trafficLight.toYellow();  expect(trafficLight.state).equal('yellow');
            trafficLight.toRed();  expect(trafficLight.state).equal('red');
        });

        it("should disallow transitioning between non-sequential states", function() {
            trafficLight.toGreen();
            expect(() => trafficLight.toRed()).to.throw('transition is invalid in current state');
            trafficLight.toYellow();
            expect(() => trafficLight.toGreen()).to.throw('transition is invalid in current state');
            trafficLight.toRed();
            expect(() => trafficLight.toYellow()).to.throw('transition is invalid in current state');
        });

        it("should output current timestamp, position and state when describe() is called", function() {
            trafficLight.toGreen();
            expect(trafficLight.describe()).to.equal(moment().format("HH:mm:ss") + ': ' + trafficLight.position + ' - ' + trafficLight.state);
        });
    });

    describe("Traffic Light Manager", function() {

        let trafficLightManager;
        let clock;

        beforeEach(function(){
            trafficLightManager = new TrafficLightManager(new TrafficLight('NS'), new TrafficLight('EW'), false);
            clock = sinon.useFakeTimers();
        });

        afterEach(function () {
            clock.restore();
        });

        it("should store each instance of traffic lights that it's managing", function() {
            expect(trafficLightManager.trafficLightNS).to.be.instanceOf(TrafficLight);
        });

        it("should transition between allowed states", function() {
            trafficLightManager.toYellowRed();  expect(trafficLightManager.state).equal('yellowRed');
            trafficLightManager.toRedGreen();   expect(trafficLightManager.state).equal('redGreen');
            trafficLightManager.toRedYellow();  expect(trafficLightManager.state).equal('redYellow');
            trafficLightManager.toGreenRed();   expect(trafficLightManager.state).equal('greenRed');
        });

        it("should disallow transitioning between non-sequential states", function() {
            trafficLightManager.toYellowRed();
            expect(() => trafficLightManager.toRedYellow()).to.throw('transition is invalid in current state');
            expect(() => trafficLightManager.toGreenRed()).to.throw('transition is invalid in current state');
            trafficLightManager.toRedGreen();
            expect(() => trafficLightManager.toGreenRed()).to.throw('transition is invalid in current state');
            expect(() => trafficLightManager.toYellowRed()).to.throw('transition is invalid in current state');
            trafficLightManager.toRedYellow();
            expect(() => trafficLightManager.toRedYellow()).to.throw('transition is invalid in current state');
            expect(() => trafficLightManager.toRedGreen()).to.throw('transition is invalid in current state');
            trafficLightManager.toGreenRed();
            expect(() => trafficLightManager.toRedYellow()).to.throw('transition is invalid in current state');
            expect(() => trafficLightManager.toRedGreen()).to.throw('transition is invalid in current state');
        });

        it("should automatically switch between states after the right timeout", function() {
            const trafficLightManager = new TrafficLightManager(new TrafficLight('NS'), new TrafficLight('EW'), true);
            trafficLightManager.init();
            expect(trafficLightManager.state).equal('yellowRed');
            clock.tick(30000);  expect(trafficLightManager.state).equal('redGreen');
            clock.tick(270000); expect(trafficLightManager.state).equal('redYellow');
            clock.tick(30000);  expect(trafficLightManager.state).equal('greenRed');
            clock.tick(270000); expect(trafficLightManager.state).equal('yellowRed');
        });
    });
});
const StateMachine = require('javascript-state-machine');
const moment = require('moment');

/**
 * Model for Traffic Light Manager
 * Parent for the Traffic Light Model, responsible for coordinating state changes between different sets of traffic lights
 *
 * State Change Diagram:
 *
 *      G -> Y -> R
 *      ^         |
 *      |         |
 *      -----------
 */

const TrafficLight = StateMachine.factory({
    data: function(position) {
        return {
            position: position
        }
    },
    transitions: [
        { name: 'toGreen', from: ['none', 'red', 'green'],  to: 'green' },
        { name: 'toYellow', from: ['none', 'green', 'yellow'], to: 'yellow' },
        { name: 'toRed', from: ['none', 'yellow', 'red'], to: 'red' },
    ],
    methods: {
        describe: function() {
            return moment().format("HH:mm:ss") + ': ' + this.position + ' - ' + this.state;
        }
    }
});

module.exports.TrafficLight = TrafficLight;